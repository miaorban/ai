import glob
import scipy.io.wavfile as wav
import numpy as np
from sklearn.cluster import DBSCAN
from numpy import random, where
import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs

zcr_x = []
zcr_y = []
zcrs = []
for filename in glob.glob('../../../sounds/corylus/*.*'):
    print(filename)
    (rate,sig) = wav.read(filename)
    zero_crosses = np.nonzero(np.diff(sig > 0))[0]
    zcr_x.append(0.0)
    zcr_y.append(zero_crosses.size/1000.0)
    zcrs.append([0.0, zero_crosses.size/1000.0])

print('zcr_x ', zcr_x)
print('zcr_y ', zcr_y)
random.seed(7)
x, _ = make_blobs(n_samples=200, centers=1, cluster_std=.3, center_box=(20, 5))

# x = zcrs
# print('x ', x)
# print('x[:,0] ', x[:,0])
plt.scatter(zcr_x, zcr_y)
plt.show()

dbscan = DBSCAN(eps = 0.28, min_samples = 20)
print(dbscan)

pred = dbscan.fit_predict(zcrs)
anom_index = where(pred == -1)
values = zcrs[anom_index]

plt.scatter(zcr_x, zcr_y)
plt.scatter(values[:,0], values[:,1], color='r')
# plt.show()

#
# plt.scatter(zcrs[:,0], zcrs[:,1])
# plt.show()
#
# dbscan = DBSCAN(eps = 0.28, min_samples = 20)
# print(dbscan)
#
# pred = dbscan.fit_predict(zcrs)
# anom_index = where(pred == -1)
# values = zcrs[anom_index]
#
# plt.scatter(zcrs[:,0], zcrs[:,1])
# plt.scatter(values[:,0], values[:,1], color='r')
# plt.show()
